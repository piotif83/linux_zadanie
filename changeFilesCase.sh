#!/bin/bash
#############################################################################
#
# Script name: changeFilesCase.sh
#
# 2018-11-16 Piotr Formela 	Created.
# 2018-11-22 Piotr Formela      Address feedbacks: handle spaces in filenames
#
#############################################################################

# handle empty spaces in filename's
IFS="
"

# read parameters, show only help information if provided "-h" or "--help"
if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
  echo -e "\nScript usage: $0 \"parameter1\" \"parameter2\" \"filename\"\n"
  echo -e "       where: parameter1 is either phrase \"upper\" or \"lower\""
  echo -e "              parameter2 is either phrase \"upper\" or \"lower\""
  echo -e "              filename is optional and points to a given file (all files if not provided)\n" 
  echo -e "\nPurpose of this script is to change filename to upper/lower case in a given file (or all files) present in a current directory."
  echo -e "In case filename is not provided in third parameter all files in current directory will be changed.\n"
  echo -e "Example run: $0 \"lower\" \"upper\" \"test.txt\"\nAbove run will replace filename \"test.txt\" in current directory to uppercase \"TEST.TXT\".\n"
  echo -e "\nHelp usage: $0 \"-h\"\n            $0 \"--help\"\n\n"
  exit 0
else if [ $# -lt 2 ] && ([ "$1" != "-h" ] || [ "$1" != "--help" ]) ; then
  echo -e "\n** Provided unexpected parameter(s). Please read help for more information (parameter -h or --help).\n"
  exit 0
 fi
fi

# main block
echo -e "\n** Executing "$0"\n"

# execute if provided third parameter (specific filename)
if [ $# = 3 ] ; then
   # loop through provided file/pattern in current directory
   for FILE in $(ls -p $3 | grep -v /)
  do
    # set new filename with changed lower/upper case using 'tr' command
    NEW_FILENAME=$(echo $FILE | tr [:$1:] [:$2:])
    echo -e "renaming file $FILE to $NEW_FILENAME"
    # rename file to the new filename (using mv command)
    $(mv $FILE $NEW_FILENAME)
  done
# execute if not provided third parameter (include all files, expect this script itself)
else if [ $# = 2 ] ; then
   for FILE in $(ls -p | grep -Ev '/|changeFilesCase.sh')
  do
    # set new filename with changed lower/upper case using 'tr' command
    NEW_FILENAME=$(echo $FILE | tr [:$1:] [:$2:])
    echo -e "renaming file $FILE to $NEW_FILENAME"
    # rename file to the new filename (using mv command)
    $(mv $FILE $NEW_FILENAME)
  done
 fi
fi

echo -e "\n** Script completed successfully.\n"

exit 0
