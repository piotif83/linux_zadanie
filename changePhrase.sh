#!/bin/bash
#########################################################################################################
#
# Script name: changePhrase.sh
#
# 2018-11-15 Piotr Formela 	Initial version
# 2018-11-16 Piotr Formela      Final version
# 2018-11-22 Piotr Formela      Addressed feedbacks: print only changed files & handle spaces in filename
#
#########################################################################################################

# handle empty spaces in filename's
IFS="
"

# read parameters, show help information if provided  "-h" or "--help" 
if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
  echo -e "\nScript usage: $0 \"parameter1\" \"parameter2\"\n"
  echo -e "       where: parameter1 is a phrase that will be changed into new one"
  echo -e "              parameter2 is a new phrase replacing defined one in first parameter\n"
  echo -e "\nPurpose of this script is to change a given phrase in all files present in a given directory and it's all subdirectories."
  echo -e "The phrase to change is provided by user in first parameter while second parameter define phrase it will be changed to."
  echo -e "Parameters are case sensitive: phrase \"hello\" is not equal to \"HELLO\"\n"
  echo -e "\nExample run: $0 \"hello\" \"Welcome\"\nAbove run will replace a phrase \"hello\" in all files found in current directory and it's subdirectries into phrase \"Welcome\".\n"
  echo -e "\nHelp usage: $0 \"-h\"\n            $0 \"--help\"\n\n"
  exit 0
else if [ $# -lt 2 ] && ([ "$1" != "-h" ] || [ "$1" != "--help" ]) ; then
  echo -e "\n** Provided unexpected parameter(s). Please read help for more information (parameter -h or --help).\n"
  exit 0
 fi
fi

# main block
echo -e "\n** Executing "$0"\n"

# loop through files in current directory and subdirectories (using find command)
for FILE in $(find . -type f | grep -v $0)
 do
  # check if file contains the phrase to be replaced
  PHRASE_EXISTS=$(grep -n "$1" $FILE | wc -l)
  if [ $PHRASE_EXISTS != 0 ] ; then
    echo -e "phrase \"$1\" to be replaced by \"$2\" in file $FILE"
    # replace phrase from parameter1 to phrase in parameter2 usind 'sed' command for given file in loop
    $(sed -i 's/'$1'/'$2'/g' $FILE)
  fi
done

echo -e "\n** Script completed successfully.\n"

exit 0
