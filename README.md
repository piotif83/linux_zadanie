# Junior Java Developer - Linux projekt zaliczeniowy
Autor: Piotr Formela, 
3 skrypty według treści zadania:


    * changePhrase - zmieniamy wskazaną w parametrze frazę na inną (również wskazaną w parametrze) w plikach znajdujących się w bieżącym 
          katalogu i jego podkatalogach.
          
          Przykład:
                $ changePhrase "okna" "drzwi"
    
    * changeFilesCase - zmieniamy wielkość liter nazw plików znajdujących się w bieżącym katalogu. Sposób zamiany ma być podany w parametrze. 
          Jeżeli nie zostanie podana nazwa pliku, przyjmuje się wszystkie pliki w katalogu.
          Przykład:
                $ changeFilesCase lower upper "test*"
    
    * setFileNumber - Skrypt numerujący wszystkie pliki w bieżącym katalogu. Wynik działania skryptu to zmiana nazwy plików na zaczynające się 
          od numeru kolejnego + podkreślenie.
          Przykład:
                1_jakistest
                2_test
                3_testinny

