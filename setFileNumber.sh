#!/bin/bash
#############################################################################
#
# Script name: setFileNumber.sh
#
# 2018-11-17 Piotr Formela 	Created.
# 2018-11-22 Piotr Formela      Addressed feedback: handle spaces in filename 
#
#############################################################################

# handle empty spaces in filename's
IFS="
"

# read parameters, show only help information if provided  "-h" or "--help" 
if [ "$1" = "-h" ] || [ "$1" = "--help" ] ; then
  echo -e "\nScript usage: $0"
  echo -e "\nScript does not require any parameters."
  echo -e "\nPurpose of this script is to add numeric (followed by underscore) in front of each file present in a current directory."
  echo -e "\nOutcome of example run will be:\n  1_jakistest\n  2_test\n  3_testinny"
  echo -e "\nHelp usage: $0 \"-h\"\n            $0 \"--help\"\n\n"
  exit 0
fi

# main block
echo -e "\n** Executing "$0"\n"

# set file counter variable to 1
FILE_NUMBER=1

# loop through files in current directory (using 'ls' command, exclude this script itself)
for FILE in $(ls -p | grep -Ev '/|setFileNumber.sh')
 do
  echo -e "file \"${FILE}\" is going to be changed to \"${FILE_NUMBER}_${FILE}\""
  # rename the file and add number in from of the file
  mv ${FILE} ${FILE_NUMBER}_${FILE}
   # increment file counter by 1
   let FILE_NUMBER "FILE_NUMBER += 1"
done

echo -e "\n** Script completed successfully.\n"

exit 0
